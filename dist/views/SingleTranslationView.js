function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React from "react";
import { withApollo, Query } from "react-apollo";
import { compose } from "recompose";
import gql from "graphql-tag";
import { withRouter } from "react-router";
import { Loading } from 'react-pe-useful';
import BasicState from "react-pe-basic-view";
import TranslationContainer from "./translationState/TranslationContainer";

class SingleTranslationView extends BasicState {
  constructor(...args) {
    super(...args);

    _defineProperty(this, "getRoute", () => "translation");
  }

  addRender() {
    const getPE_Translation = gql`query getPE_Translation($id:String)
		{
			getPE_Translation(id:$id)
			{
				id 
				post_title 
				post_content 
				start_date 
				end_date 
				external_id 
				external_title 
				is_locked 				
				post_author
				{
					id
					display_name
					avatar
				}
				pe_room
				{
					id 
					post_title 
					post_content 
					external_id 
					is_locked 
					password 
					post_author
					{
						id
						display_name
						avatar
					}
				} 
			}
		}`;
    return /*#__PURE__*/React.createElement(Query, {
      query: getPE_Translation,
      variables: {
        id: this.props.match.params.id
      }
    }, ({
      loading,
      data,
      error
    }) => {
      if (loading) {
        return /*#__PURE__*/React.createElement(Loading, null);
      }

      if (data) {
        // console.log( data.getPE_Translation );
        const translation = data.getPE_Translation || {};
        return /*#__PURE__*/React.createElement(TranslationContainer, _extends({}, this.props, {
          translation: translation
        }));
      }
    });
  }

}

export default compose(withApollo, withRouter)(SingleTranslationView);