function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

import React, { Component } from "react";
import { Button, Dialog } from "@blueprintjs/core";
import gql from "graphql-tag";
import { withApollo } from "react-apollo";
import { compose } from "recompose";
import getJitsiExternalId from "../views/translationState/getJitsiExternalId";
import { __ } from "react-pe-utilities";

class StartTranslation extends Component {
  constructor(props) {
    super(props);

    _defineProperty(this, "onChange", (field, value) => {
      const state = { ...this.state
      };
      state[value] = field;
      this.setState(state, () => {
        console.log(this.state); // this.props.on(field, value);
      });
    });

    _defineProperty(this, "onDelete", () => {});

    _defineProperty(this, "onClose", () => {});

    _defineProperty(this, "onTitle", evt => {
      this.setState({
        post_titletitle: evt.currentTarget.value,
        external_title: evt.currentTarget.value.split(" ").join("")
      });
    });

    _defineProperty(this, "onDescr", evt => {
      console.log(evt.currentTarget.value.split(" ").join(""));
      this.setState({
        post_content: evt.currentTarget.value,
        external_id: evt.currentTarget.value.split(" ").join("")
      });
    });

    _defineProperty(this, "onDescrText", text => {
      this.setState({
        post_content: text,
        external_id: text.split(" ").join("")
      });
    });

    _defineProperty(this, "onOpenDialog", () => {
      this.setState({
        isOpen: !this.state.isOpen
      });
    });

    _defineProperty(this, "onStart", () => {
      const changePE_Translation = gql`mutation changePE_Translation( $input: PE_TranslationInput ) 
		{
			changePE_Translation( input: $input ) 
			{
				id
				post_title
				__typename
			}
		}`;
      const pr = { ...this.state
      };
      pr.external_id = getJitsiExternalId(pr.post_title);
      pr.start_date = parseInt(new Date(pr.start_date).getTime() / 1000);
      pr.end_date = parseInt(new Date(pr.end_date).getTime() / 1000);
      delete pr.isOpen;
      this.props.client.mutate({
        mutation: changePE_Translation,
        variables: {
          input: pr
        },
        update: (store, {
          data: {
            changePE_Translation
          }
        }) => {
          // console.log( changePE_Translation.id );
          this.props.onStart(changePE_Translation.id);
        }
      });
      /*
      	const date = ( new Date() ).toString();
      	const md = md5( date );
      	const id = ( md ).substring( 0, 5 );
      	const roomId = (md5( "rand" + date )).substring( 0, 5 );
      	this.props.onStart( id );
      	*/
    });

    this.state = {
      post_title: __("New Translation"),
      external_title: __("NewTranslation"),
      post_content: __("this is Translation"),
      external_id: __("thisisTranslation"),
      // geo				: [55.76, 37.64],
      is_locked: false,
      isOpen: false
    };
  }

  render() {
    // console.log(this.props);
    return /*#__PURE__*/React.createElement(React.Fragment, null, /*#__PURE__*/React.createElement("div", {
      className: "btn btn-danger btn-large btn-block",
      onClick: this.onOpenDialog
    }, __("Start new Translation")), /*#__PURE__*/React.createElement(Dialog, {
      isOpen: this.state.isOpen,
      title: __("Settings of Translation"),
      onClose: this.onOpenDialog,
      style: {
        zIndex: -1
      },
      className: "booo"
    }, /*#__PURE__*/React.createElement("div", {
      className: "d-flex flex-column justify-content-center p-5"
    }, /*#__PURE__*/React.createElement(Button, {
      onClick: this.onStart,
      className: "mt-2"
    }, __("Create")))));
  }

}

export default compose(withApollo)(StartTranslation);