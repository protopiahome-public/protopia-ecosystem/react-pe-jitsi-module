import React from "react";

const Transactions = props => {
  console.log(props);
  return props.value ? /*#__PURE__*/React.createElement("div", {
    className: "row dat"
  }, /*#__PURE__*/React.createElement("div", {
    className: "col-md-3  layout-label"
  }, props.title), /*#__PURE__*/React.createElement("div", {
    className: "col-md-9 layout-data",
    style: {
      position: "relative"
    }
  }, /*#__PURE__*/React.createElement("div", {
    className: "w-100 p-3"
  }, /*#__PURE__*/React.createElement("div", {
    className: "row"
  }, /*#__PURE__*/React.createElement("div", {
    className: "col-md-5 py-2 text-light bg-secondary"
  }, "\u041F\u043E\u043A\u0443\u043F\u043A\u0430"), /*#__PURE__*/React.createElement("div", {
    className: "col-md-4 py-2 text-light bg-secondary"
  }, "\u041A\u043E\u043C\u043C\u0435\u043D\u0442\u0430\u0440\u0438\u0439 \u043A \u043F\u043B\u0430\u0442\u0435\u0436\u0443"), /*#__PURE__*/React.createElement("div", {
    className: "col-md-2 py-2 text-light bg-secondary"
  }, "\u0421\u0443\u043C\u043C\u044B"), /*#__PURE__*/React.createElement("div", {
    className: "col-md-1 py-2 text-light bg-secondary"
  }, "\u0414\u0430?")), props.value.map((e, k) => /*#__PURE__*/React.createElement("div", {
    key: k,
    className: "row dat2"
  }, /*#__PURE__*/React.createElement("div", {
    className: "col-md-5 py-1"
  }, e.post_title), /*#__PURE__*/React.createElement("div", {
    className: "col-md-4 py-1"
  }, e.post_content), /*#__PURE__*/React.createElement("div", {
    className: "col-md-2 py-1"
  }, e.summae), /*#__PURE__*/React.createElement("div", {
    className: "col-md-1 py-1"
  }, e.is_success ? /*#__PURE__*/React.createElement("i", {
    className: "fas fa-check text-success"
  }) : /*#__PURE__*/React.createElement("i", {
    className: "fas fa-times text-danger"
  }))))))) : null;
};

export default Transactions;