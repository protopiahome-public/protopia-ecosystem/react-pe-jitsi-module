import React from "react"
import { withApollo, Query } from "react-apollo"
import { compose } from "recompose"
import gql from "graphql-tag"
import { withRouter } from "react-router"
import {Loading} from 'react-pe-useful'
import BasicState from "react-pe-basic-view"
import TranslationContainer from "./translationState/TranslationContainer"

class SingleTranslationView extends BasicState {
	/*
	constructor(props)
	{
		super(props);
		this.state = { }
		console.log( (new Date(new Date().getTime() + 24 * 60 * 60 * 1000)).getTime()/1000 );
	}
	*/
	getRoute = () => "translation"

	addRender() {
		const getPE_Translation = gql`query getPE_Translation($id:String)
		{
			getPE_Translation(id:$id)
			{
				id 
				post_title 
				post_content 
				start_date 
				end_date 
				external_id 
				external_title 
				is_locked 				
				post_author
				{
					id
					display_name
					avatar
				}
				pe_room
				{
					id 
					post_title 
					post_content 
					external_id 
					is_locked 
					password 
					post_author
					{
						id
						display_name
						avatar
					}
				} 
			}
		}`

		return (
			<Query query={getPE_Translation} variables={{ id: this.props.match.params.id }}>
				{
					({ loading, data, error }) => {
						if (loading) {
							return <Loading />
						}
						if (data) {
							// console.log( data.getPE_Translation );
							const translation = data.getPE_Translation || {}
							return (
								<TranslationContainer
									{...this.props}
									translation={translation}
								/>
							)
						}
					}
				}
			</Query>
		)
	}
}

export default compose(
	withApollo,
	withRouter,
)(SingleTranslationView)
